# CURSO EAyO: Introducción a las Bases de Datos

Clases Virtuales: Jueves de 17 a 20hs <br> 

**Temario**

[[_TOC_]]

## Clase 1: Fundamentos
- [Diapositiva](https://gitlab.com/ciorozco/2021-eayo-bbdd/-/blob/main/Recursos/2021EAyO_BBDD_Cls_1.pdf) <br>
- [Video - Clase grabada](https://www.youtube.com/watch?v=-zPMDZW92Nk) <br>

## Clase 2: DER
- [Diapositiva](https://gitlab.com/ciorozco/2021-eayo-bbdd/-/blob/main/Recursos/2021EAyO_BBDD_Cls_2.pdf) <br>
- [Video - Clase grabada](https://youtu.be/43PoTWvdx4g) <br>

## Clase 3: DER - Relaciones
- [Diapositiva](https://gitlab.com/ciorozco/2021-eayo-bbdd/-/blob/main/Recursos/2021EAyO_BBDD_Cls_3.pdf) <br>
- [Video - Clase grabada](https://youtu.be/LHY4b3CWBuY) <br>

## Clase 4: Ejercicios
- [Diapositiva](https://gitlab.com/ciorozco/2021-eayo-bbdd/-/blob/main/Recursos/2021EAyO_BBDD_Cls_4.pdf) <br>
- [Video - Clase grabada](https://www.youtube.com/watch?v=eMXs5uwjOEY) <br>

## Clase 5: Introducción a Libre Office Base
- [Video - Clase grabada](https://youtu.be/CciU9iuxwso) <br>

## Clase 6: Caso de Estudio: Registro de asistencias EAyO
- [Video - Clase grabada](https://youtu.be/JKaLOkdqryQ) <br>

## Clase 7: Introducción al TP Final
- [Video - Clase grabada](https://youtu.be/meOy9nX2V9A) <br>

1. Seleccionar un Caso de Estudio particular. Describa las actividades principales que se van a tener en cuenta del mismo.

2. Diseñar un Diagrama de Entidad-Relación para el caso de estudio propuesto.

3. Empleando Libre Office Base, Realizar la implementación del DER

4. Implementar Formularios

5. Implementar Reportes

